//
//  AGTWikiViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 20/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

#import "AGTStarWarsCharacter.h"

@interface AGTWikiViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) AGTStarWarsCharacter *model;
@property (nonatomic, weak) IBOutlet UIWebView *browser;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityView;


-(id) initWithModel:(AGTStarWarsCharacter *) model;

@end
