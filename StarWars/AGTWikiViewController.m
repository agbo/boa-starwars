//
//  AGTWikiViewController.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 20/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTWikiViewController.h"
#import "AGTUniverseViewController.h"

@implementation AGTWikiViewController
-(id) initWithModel:(AGTStarWarsCharacter *) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
        self.title = @"Wikipedia";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Nos aseguramos de que la vista no ocupe toda
    // la pantalla (novedad de iOS7)
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // Sincronizar modelo -> vista
    [self syncWithModel];
    
    // Alta en notificaciones
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(notifyThatCharacterDidChange:)
               name:CHARACTER_DID_CHANGE_NOTIFICATION
             object:nil];

}
// CHARACTER_DID_CHANGE_NOTIFICATION
-(void) notifyThatCharacterDidChange:(NSNotification *)n{
    
    // ¡Mi modelo ha cambiado!
    
    // Obtengo el modelo que está en la notificación
    AGTStarWarsCharacter *character = [n.userInfo objectForKey:CHARACTER_KEY];
    
    // Actualizo el modelo
    self.model = character;
    
    // sincronizo
    [self syncWithModel];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // Baja en notificaciones
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIWebViewDelegate
-(void) webViewDidFinishLoad:(UIWebView *)webView{
    
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
}

-(BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
navigationType:(UIWebViewNavigationType)navigationType{
    
    if ((navigationType == UIWebViewNavigationTypeLinkClicked) ||
        (navigationType == UIWebViewNavigationTypeFormSubmitted)){
        return NO;
    }else{
        return YES;
    }
    
}

#pragma mark -  Utils
-(void) syncWithModel{
    
    self.browser.delegate = self;
    
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    
    [[self browser] loadRequest:
     [NSURLRequest requestWithURL:[[self model] wikiPage]]];
    
    [self.browser loadRequest:
     [NSURLRequest requestWithURL:self.model.wikiPage]];
}















@end
