//
//  AGTStarWarsCharacter.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface AGTStarWarsCharacter : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *alias;
@property (strong, nonatomic) NSURL *wikiPage;
@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) NSData *soundData;

+(instancetype)starWarsCharacterWithName:(NSString *) name
                                   alias:(NSString *) alias
                                     wikiPage:(NSURL*) wikiPage
                                   image:(UIImage *) image
                               soundData:(NSData *) soundData;

+(instancetype)starWarsCharacterWithAlias:(NSString *) alias
                                      wikiPage:(NSURL*) wikiPage
                                    image:(UIImage *) image
                                soundData:(NSData *) soundData;

// designated
-(id) initWithName:(NSString *) name
             alias:(NSString *) alias
               url:(NSURL*) wikiPage
             image:(UIImage *) image
         soundData:(NSData *) soundData;

-(id) initWithAlias:(NSString *) alias
                url:(NSURL*) wikiPage
              image:(UIImage *) image
          soundData:(NSData *) soundData;

@end








