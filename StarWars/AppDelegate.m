//
//  AppDelegate.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AppDelegate.h"

#import "AGTStarWarsCharacter.h"
#import "AGTCharacterViewController.h"
#import "AGTWikiViewController.h"
#import "AGTStarWarsUniverse.h"
#import "AGTUniverseViewController.h"
#import "Settings.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Valores por defecto de preferencias de usuario
    [self setDefaultPreferences];
    
    // Creamos la Window
    [self setWindow:[[UIWindow alloc]
                     initWithFrame:[[UIScreen mainScreen]bounds]]];
    
    [[self window] setBackgroundColor:[UIColor orangeColor]];

    
    // Creamos el modelo
    AGTStarWarsUniverse *model = [AGTStarWarsUniverse new];
    
    
    // Averiguo el tipo de interfaz (tableta o teléfono)
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // Tableta
        [self configureForiPadWithModel: model];
        
    }else{
        // Teléfono (incluye el iPhone 6+)
        [self configureForiPhoneWithModel: model];

        
    }
    
    
    // Mostramos las vistas
    [[self window] makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) configureForiPadWithModel:(AGTStarWarsUniverse*)model{
    
    // Creamos los controladores
    AGTUniverseViewController *uVC = [[AGTUniverseViewController alloc] initWithModel:model style:UITableViewStylePlain];
    // Este lo cargo con el último personaje seleccionado
    AGTCharacterViewController *charVC = [[AGTCharacterViewController alloc] initWithModel:[self lastCharacterInModel:model]];
    
    // Creamos los navigations
    UINavigationController *navU = [UINavigationController new];
    [navU pushViewController:uVC animated:NO];
    
    UINavigationController *navChar = [UINavigationController new];
    [navChar pushViewController:charVC animated:NO];
    
    // Creamos el combinador
    UISplitViewController *splitVC = [UISplitViewController new];
    splitVC.viewControllers = @[navU, navChar];
    
    
    // Asignamos delegados
    splitVC.delegate = charVC;
    uVC.delegate = charVC;
    
    // Lo hacemos el root
    [[self window] setRootViewController:splitVC];
    
}

-(void) configureForiPhoneWithModel:(AGTStarWarsUniverse*)model{
    
    // Creamos el controlador
    AGTUniverseViewController *uVC = [[AGTUniverseViewController alloc] initWithModel:model style:UITableViewStylePlain];
    
    // Creamos el combinador
    UINavigationController *navVC = [UINavigationController new];
    [navVC pushViewController:uVC
                     animated:NO];
    
    // Asignamos delegados
    uVC.delegate = uVC;
    
    // Lo hacemos el root
    self.window.rootViewController = navVC;
    
}

#pragma mark - Preferences
-(void) setDefaultPreferences{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    if (![def valueForKey:LAST_CHARACTER]) {
        // No hay nada: ponemos valores por defecto
        
        [def setObject:@[@0, @0]
                forKey:LAST_CHARACTER];
        
        // por si acaso, guardo
        [def synchronize];
    }
}

-(AGTStarWarsCharacter*) lastCharacterInModel:(AGTStarWarsUniverse*)u{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSArray *coords = [def objectForKey:LAST_CHARACTER];
    
    NSUInteger section = [[coords objectAtIndex:0] integerValue];
    NSUInteger index = [[coords objectAtIndex:1] integerValue];
    
    if (section == IMPERIAL_SECTION) {
        return [u imperialAtIndex:index];
    }else{
        return [u rebelAtIndex:index];
    }
    
}




























@end
