//
//  AGTCharacterViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

#import "AGTStarWarsCharacter.h"
#import "CafPlayer.h"
#import "AGTUniverseViewController.h"

@interface AGTCharacterViewController : UIViewController <UISplitViewControllerDelegate, AGTUniverseViewControllerDelegate>


@property (nonatomic, strong) AGTStarWarsCharacter *model;
@property (nonatomic, strong) CafPlayer *player;

@property (nonatomic, weak) IBOutlet UIImageView *photoView;


-(id) initWithModel:(AGTStarWarsCharacter*) model;


-(IBAction)playSound:(id)sender;
-(IBAction)displayWiki:(id)sender;

@end
