//
//  AGTUniverseViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 20/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;
#import "AGTStarWarsUniverse.h"

#define IMPERIAL_SECTION 0
#define REBEL_SECTION 1

#define CHARACTER_DID_CHANGE_NOTIFICATION @"characterNotification"
#define CHARACTER_KEY @"characterKey"

@class AGTUniverseViewController;

@protocol AGTUniverseViewControllerDelegate <NSObject>

@optional
-(void)universeViewController:(AGTUniverseViewController*)uVC
           didSelectCharacter:(AGTStarWarsCharacter*) character;

@end



@interface AGTUniverseViewController : UITableViewController <AGTUniverseViewControllerDelegate>

@property (nonatomic, strong) AGTStarWarsUniverse *model;
@property (nonatomic, weak) id<AGTUniverseViewControllerDelegate> delegate;



-(id) initWithModel:(AGTStarWarsUniverse *) model
              style:(UITableViewStyle) style;

@end


