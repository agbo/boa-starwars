//
//  AGTStarWarsCharacter.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTStarWarsCharacter.h"

@implementation AGTStarWarsCharacter

+(instancetype)starWarsCharacterWithName:(NSString *) name
                                   alias:(NSString *) alias
                                     wikiPage:(NSURL*) wikiPage
                                   image:(UIImage *) image
                               soundData:(NSData *) soundData{
    
    return [[self alloc] initWithName:name
                                alias:alias
                                  url:wikiPage
                                image:image
                            soundData:soundData];
}

+(instancetype)starWarsCharacterWithAlias:(NSString *) alias
                                      wikiPage:(NSURL*) wikiPage
                                    image:(UIImage *) image
                                soundData:(NSData *) soundData{
    
    return [[self alloc] initWithAlias:alias
                                   url:wikiPage
                                 image:image
                             soundData:soundData];
}

// designated
-(id) initWithName:(NSString *) name
             alias:(NSString *) alias
               url:(NSURL*) wikiPage
             image:(UIImage *) image
         soundData:(NSData *) soundData{
    
    if (self = [super init]) {
        
        // guardar parámetros en propiedades
        _name = name;
        _alias = alias;
        _wikiPage = wikiPage;
        _photo = image;
        _soundData = soundData;
    }
    
    return self;
}

-(id) initWithAlias:(NSString *) alias
                url:(NSURL*) wikiPage
              image:(UIImage *) image
          soundData:(NSData *) soundData{
    
    return [self initWithName:nil
                        alias:alias
                          url:wikiPage
                        image:image
                    soundData:soundData];
}

@end










