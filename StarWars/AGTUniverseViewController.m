//
//  AGTUniverseViewController.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 20/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTUniverseViewController.h"
#import "AGTCharacterViewController.h"
#import "Settings.h"

@interface AGTUniverseViewController ()

@end

@implementation AGTUniverseViewController

-(id) initWithModel:(AGTStarWarsUniverse *) model
              style:(UITableViewStyle) style{
    
    if (self = [super initWithStyle:style]) {
        _model = model;
        self.title = @"Star Wars Universe";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    if (section == IMPERIAL_SECTION) {
        return self.model.imperialCount;
    }else{
        return self.model.rebelCount;
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellId = @"CharacterCell";
    
    // Averiguar de qué personaje me está hablando
    AGTStarWarsCharacter *character = nil;
    if (indexPath.section == IMPERIAL_SECTION) {
        character = [self.model imperialAtIndex:indexPath.row];
    }else{
        character = [self.model rebelAtIndex:indexPath.row];
    }
    
    // Crear la celda
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        
        // crearla de cero
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    // Configurarla
    // Sincronizamos modelo (character) -> vista (cell)
    cell.imageView.image = character.photo;
    cell.textLabel.text = character.alias;
    cell.detailTextLabel.text = character.name;
    
    // Devolverla
    return cell;
    
}

-(NSString*) tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section{
 
    if (section == IMPERIAL_SECTION) {
        return @"Empire";
    }else{
        return @"Rebel Alliance";
    }
}


#pragma mark - TableView Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar de qué personaje me estás hablando
    AGTStarWarsCharacter *character;
    if (indexPath.section == IMPERIAL_SECTION) {
        character = [self.model imperialAtIndex:indexPath.row];
    }else{
        character = [self.model rebelAtIndex:indexPath.row];
    }
    
    
    // Aviso al delegado
    if ([self.delegate respondsToSelector:@selector(universeViewController:didSelectCharacter:)]) {
        
        // Sí que lo entiende, así que se lo mando
        [self.delegate universeViewController:self
                           didSelectCharacter:character];
    }
    
    // Aviso también mediante notificaciones
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    NSDictionary *userInfo = @{CHARACTER_KEY : character};
    
    NSNotification *n = [NSNotification notificationWithName:CHARACTER_DID_CHANGE_NOTIFICATION object:self userInfo:userInfo];
    
    [nc postNotification:n];
    
    // Guardo las coordenadas del último personaje
    [self saveLastCharacterAtSection:indexPath.section
                              andRow:indexPath.row];
    
    
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - AGTUniverseViewControllerDelegate
-(void) universeViewController:(AGTUniverseViewController *)uVC
            didSelectCharacter:(AGTStarWarsCharacter *)character{
    
    // Creamos el controlador
    AGTCharacterViewController *charVC = [[AGTCharacterViewController alloc] initWithModel:character];
    
    
    // Hacemos el push
    [self.navigationController pushViewController:charVC
                                         animated:YES];
    
    
}

#pragma mark - Preferences
-(void) saveLastCharacterAtSection:(NSUInteger) section
                            andRow:(NSUInteger) row{
 
    
    // Crear el NSUserDefaults
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    // Actualizar coordenadas
    [def setObject:@[@(section), @(row)]
            forKey:LAST_CHARACTER];
    
    // Guardar
    [def synchronize];
    
}


















@end
