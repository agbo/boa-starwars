//
//  AGTCharacterViewController.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 19/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCharacterViewController.h"
#import "AGTWikiViewController.h"


@implementation AGTCharacterViewController

-(id) initWithModel:(AGTStarWarsCharacter*) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
        self.title = model.alias;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // sincronizamos modelo -> vista
    [self syncWithModel];
    
    
    // Nos aseguramos de que la vista no ocupe toda
    // la pantalla (novedad de iOS7)
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(IBAction)playSound:(id)sender{
    
    [self setPlayer:[CafPlayer cafPlayer]];
    [[self player] playSoundData:[[self model]soundData]];
    
}

-(IBAction)displayWiki:(id)sender{
    
    // Crear un WikiVC
    AGTWikiViewController *wikiVC = [[AGTWikiViewController alloc] initWithModel:self.model];
    
    // Hacer un push al Navigation
    [self.navigationController pushViewController:wikiVC
                                         animated:YES];
    
}

#pragma mark - UISplitViewControllerDelegate
-(void) splitViewController:(UISplitViewController *)svc
    willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode{
    
    if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
        // Nos vamos a vertical
        self.navigationItem.leftBarButtonItem = svc.displayModeButtonItem;
    }else{
        self.navigationItem.leftBarButtonItem = nil;
    }
}

#pragma mark - AGTUniverseViewControllerDelegate
-(void) universeViewController:(AGTUniverseViewController *)uVC
            didSelectCharacter:(AGTStarWarsCharacter *)character{
    
    // Nuestro modelo ha cambiado
    
    // actualizo el modelo
    self.model = character;
    
    // Sincronizo modelo -> vista
    [self syncWithModel];
   
}

#pragma mark - Utils
-(void) syncWithModel{
    
    self.title = self.model.alias;
    self.photoView.image = self.model.photo;
}





















@end
